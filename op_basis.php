<?php

class Person{

    public $name;
    public $dateOfBirth;

    protected $protectedValue;


    public function setName($localName)
    {
        $this->name = $localName;
        $this->protectedValue = 100;
    }

    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
        echo $this->protectedValue;
    }

    public function eatSomething()
    {

        echo "I am". $this->name. "I'm eating now....<br>";

    }

    public function doSomeWork()
    {
        echo "I am". $this->name. "I'm doing some work....<br>";
    }

} // end of person class


$objPerson = new Person();
$objPerson->setName("Mukta");
echo $objPerson->name;

echo "<br>";
$objPerson->setDateOfBirth("22-9-1994");
echo $objPerson->dateOfBirth;

echo "<br>";
$objPerson->eatSomething();
$objPerson->doSomeWork();
